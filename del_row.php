<?php

if (isset($_GET['id_riga']) && !db_is_null($_GET['id_riga'])) {
    if (isset($_GET['action']) && !db_is_null($_GET['action']) && $_GET['action'] == 'del') {
        $carrello->deleteRow($_GET['id_riga']);
        echo "<script>window.location.href='cart.php';</script>";
    }
    
    if (isset($_GET['action']) && !db_is_null($_GET['action']) && $_GET['action'] == 'upd') {
        if (isset($_GET['qta']) && !db_is_null($_GET['qta'])) {
            $carrello->updateRow($_GET['id_riga'], $_GET['qta']);
            echo "<script>window.location.href='cart.php';</script>";
        }
        
    }
    
    echo "<script>window.location.href='cart.php';</script>";
}

