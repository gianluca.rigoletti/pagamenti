<?php

include_once 'Librerie/start_db.php';
include_once 'Librerie/ges_db.php';

class Cart {
    private $id;
    private $nome;
    private $cognome;
    private $email;
    private $note;
    private $acquistato;
    private $rows = array();
    private $shipping_data = array();

    /**
     * Default constructor.
     * It takes no argumetn fro a new cart
     * and a $id for an existing cart.
     */
    function __construct() {
        $this->acquistato = 0;
        $this->cognome = null;
        $this->email = null;
        $this->note = null;

        $a=  func_get_args();
        if (!empty($a)) {
            $this->id = $a[0];
            //Fetch and set rows
            $query_rows = db_query_generale('dettaglio_carrello', 'IDCARRELLO = '.$this->id, 'ID');
            while ($row = mysql_fetch_assoc($query_rows)) {
                $this->rows[] =  $row;
            }
            // Fetch and set other fields
            $query_cart = db_query_mod('carrello', $this->id);
            $res_cart = mysql_fetch_assoc($query_cart);
            $this->nome = $res_cart['NOME'];
            $this->cognome = $res_cart['COGNOME'];
            $this->email = $res_cart['EMAIL'];
            $this->note = $res_cart['NOTE'];

        } else {
            $sql = "INSERT INTO carrello (ID, NOME, COGNOME, EMAIL, NOTE, ACQUISTATO) VALUES ("
                . " null, null, null, null, null, ".$this->acquistato." )";
            $query = mysql_query($sql);
            $this->id = db_get_id();
        }
        $_SESSION['id_cart'] = $this->id;
    }

    /**
     * Get Cart Object from its id
     *
     * @param type $id
     * @return \Cart
     */
    function getCart($id) {
        //Fetch and set from db
        $query = db_query_mod('carrello', $id);
        $res = mysql_fetch_assoc($query);
        $this->id         = $res['ID'];
        $this->nome       = $res['NOME'];
        $this->cognome    = $res['COGNOME'];
        $this->email      = $res['EMAIL'];
        $this->acquistato = $res['ACQUISTATO'];
        //Fetch and set rows
        $query_rows = db_query_generale('dettaglio_carrello', 'IDCARRELLO = '.$id, 'ID');
        while ($row = mysql_fetch_assoc($query_rows)) {
            $this->rows[] =  $row;
        }


        return $this;
    }

    /**
     * Update the cart if new rows are inserted.
     *
     */
    function updateCart() {
        $rows = array();
        //Fetch and set from db
        $query = db_query_mod('carrello', $this->id);
        $res = mysql_fetch_assoc($query);
        $this->nome       = $res['NOME'];
        $this->cognome    = $res['COGNOME'];
        $this->email      = $res['EMAIL'];
        $this->acquistato = $res['ACQUISTATO'];
        //Fetch and set rows
        $query_rows = db_query_generale('dettaglio_carrello', 'IDCARRELLO = '.$this->id, 'ID');
        while ($row = mysql_fetch_assoc($query_rows)) {
            $rows[] =  $row;
        }
        $this->rows = $rows;

    }

    /**
     * Insert a new row in the cart
     *
     * @param type array()
     *
     */
    function insertRow($site, $payment_id, $quantity = 1, $desc = null, $price = null) {
        //Check if the payment id is already in the cart
        $update = false;
        $rows = $this->getRows();
        foreach ($rows as $row) {
            if ($row['IDPAGAMENTI'] == $payment_id) {
                //row exists. Update the db instead of inserting a new row
                //Get the existing quantity
                $query = db_query_mod('dettaglio_carrello', $row['ID']);
                $res = mysql_fetch_assoc($query);
                $new_quantity = $quantity + $res['QTA'];
                //Check if id = 0
                if($payment_id == 0) {
                    //Update the description and price
                    $res['DESCRIZIONE'] = $desc;
                    $res['PREZZO'] = db_visimporti($price);
                    $res['QTA'] = $quantity;
                } else {
                    $res['QTA'] = $new_quantity;
                }
                //Update the db
                db_update('dettaglio_carrello', $res['ID'], $res);
                //Update the cart with the new rows
                $this->updateCart();
                $update = true;
            }
        }
        //New row to insert
        if (!$update) {
            //If already set, get desc and price from pagamenti
            if ($payment_id != 0) {
                $query = db_query_mod('pagamenti', $payment_id);
                $res = mysql_fetch_assoc($query);
                $desc = $res['PRODOTTO'];
                $price = db_visimporti($res['PREZZO']);
            }
            //Insert row in db and add to the attribute of the class
            db_insert('dettaglio_carrello', array(
                'ID' => 'null',
                'IDCARRELLO' => $this->id,
                'IDPAGAMENTI' => $payment_id,
                'QTA' =>$quantity,
                'SITO' => $site,
                'DESCRIZIONE' => $desc,
                'PREZZO' => $price
            ));
            //Get the row inserted in db
            $query = db_query_mod('dettaglio_carrello', db_get_id());
            $res = mysql_fetch_assoc($query);
            $this->rows[] = $res;
            $this->updateCart();
            //die(print_r($this->rows).'@@@@'.var_dump($this).'#####'.var_dump($res));
        }

    }


    /**
     * Search for a row and delete it.
     * It returns true if the row is found
     * or false otherwise.
     *
     * @param type $id
     * @return boolean
     */
    function deleteRow($id) {
        //Search for the row with desired id
        for ($i = 0; $i < count($this->rows); $i++) {
            if ($this->rows[$i]['ID'] == $id) {
                unset($this->rows[$i]);
                db_delete('dettaglio_carrello', $id);
                return true;
            }
        }
        $this->updateCart();
        //Row not found
        return false;

    }

    /**
     * Set the quantity of a row given the id.
     * Return true if succesful, false otherwise
     *
     * @param type $id
     * @param type $quantity
     */
    function updateRow($id, $quantity) {
        for ($i = 0; $i < count($this->rows); $i++) {
            if ($this->rows[$i]['ID'] == $id) {
                $this->rows[$i]['QTA'] = $quantity;
                db_update('dettaglio_carrello', $id, $this->rows[$i]);
                $this->updateCart();
                return true;
            }
        }
        $this->updateCart();
        return false;
    }

    static function saveToDatabase($cart) {
        $acquistato = ($cart->acquistato == 0)? FALSE : TRUE;
        $record = array(
            'ID' => $cart->id,
            'NOME' => $cart->nome,
            'COGNOME' => $cart->cognome,
            'EMAIL' => $cart->email,
            'NOTE' => $cart->note,
            'ACQUISTATO' => $acquistato
        );
        db_update('carrello',$cart->id ,$record);
    }

    static function getSpese() {
    	$default_path = 'config.json';
    	$string = file_get_contents($default_path);
    	$json = json_decode($string, true);
    	return $json['spese'];
    }

    function getTotal() {
      $total = 0;
      $sub_total = 0;
      $shipping_fees = Cart::getSpese();
      $price = 0;
      foreach ($this->getRows() as $row) {
        $pagamento = db_query_mod('pagamenti', $row['IDPAGAMENTI']);
        $res_pagamento = mysql_fetch_assoc($pagamento);
        $price = $row['PREZZO']*$row['QTA'];
        if ($res_pagamento['SPESE_SPEDIZIONE'] == 0) {
          $sub_total = $row['PREZZO']*$row['QTA'];
        } else {
          $sub_total = $row['PREZZO']*$row['QTA'] + $shipping_fees;
        }
        $total += $sub_total;
      }

      return $total;
    }

    /**
    *Determines if the current cart has at least one row with shipping costs
    *
    *@return boolean
    */
    function hasShippingFees() {
      $rows = $this->getRows();
      foreach ($rows as $row) {
        $payment = db_query_mod('pagamenti', $row['IDPAGAMENTI']);
        $res_payment = mysql_fetch_assoc($payment);
        if ($res_payment['SPESE_SPEDIZIONE'] == 1) {
          return true;
        }
      }
      return false;
    }

    /**
    *Sets shipping data on database for the current cart
    *
    */
    function setShippingData($data) {
      //Check first if the cart has already shipping data
      if ($this->hasShippingData()) {
        db_update('spedizioni', $this->getId(), $data);
      } else {
        $data['IDCARRELLO'] = $this->getId();
        db_insert('spedizioni', $data);
      }
    }

    /**
    *Checks if the current cart has a record on shipping Row
    *
    *@return boolean
    */
    function hasShippingData() {
      $data = db_query_generale('spedizioni', 'IDCARRELLO = '.$this->getId(),'id');
      $num_rows = mysql_num_rows($data);
      if ($num_rows == 0) {
        return false;
      }
      return true;
    }

    /**
    *Gets the row of shipping data associated to the current cart
    *
    *@return type array()
    */
    function getShippingData() {
      $data = db_query_generale('spedizioni', 'IDCARRELLO = '.$this->getId(),'id');
      $res_data = mysql_fetch_assoc($data);
      return $res_data;
    }

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getCognome() {
        return $this->cognome;
    }

    function getEmail() {
        return $this->email;
    }

    function getNote() {
        return $this->note;
    }

    function getAcquistato() {
        return $this->acquistato;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setCognome($cognome) {
        $this->cognome = $cognome;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setNote($note) {
        $this->note = $note;
    }

    function setAcquistato($acquistato) {
        $this->acquistato = $acquistato;
    }

    function getRows() {
        return $this->rows;
    }

    function setRows($rows) {
        $this->rows = $rows;
    }


}

?>
