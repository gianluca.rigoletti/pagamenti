<?php

require '../../Librerie/connect.php';
require '../../Librerie/html.php';
require '../../Librerie/files.php';
require '../../Librerie/i18n.php';    
require '../../Librerie/configurazione.php';

$c_files = new files();   

$Tavola= "carrello";


if ($_GET['p_upd']==1) {
 $Funzione = "Update";
 $Disabilita_chiave = "disabled";
 $Titolo = "Modifica Carrello";
} else {
 $Funzione = "Insert";
 $Disabilita_chiave = "";
 $Titolo = "Nuovo Carrello";
}

// se richiamato in update allora devo popolare il form
if ($_GET['p_upd']==1) {
 $risultato = db_query_mod($Tavola,$_GET['p_id']);
 $cur_rec = mysql_fetch_assoc($risultato);
 //$cur_rec['SCADENZA_VIS'] = db_converti_data($cur_rec['SCADENZA']);
}

// confermo
if ( isset($_POST['Insert']) || isset($_POST['Update']) ) {

 $cur_rec['ID'] = $_POST['ID'];
 $cur_rec['NOME'] = $_POST['NOME'];
 $cur_rec['COGNOME'] = $_POST['COGNOME'];
 $cur_rec['NOTE'] = $_POST['NOTE'];
 $cur_rec['EMAIL'] = $_POST['EMAIL'];
 $cur_rec['ACQUISTATO'] = $_POST['ACQUISTATO'];

if ($_POST['NOME'] == null || $_POST['NOME'] == " ") {
 $c_err->add("Campo NOME Obbligatorio","NOME");
}

if ($_POST['EMAIL'] == null || $_POST['EMAIL'] == " ") {
  $c_err->add("Campo EMAIL Obbligatorio","EMAIL");
}

if ($_POST['COGNOME'] == null || $_POST['COGNOME'] == " ") {
  $c_err->add("Campo COGNOME Obbligatorio","COGNOME");
} 

print_r($_POST);
   // controllo dup-Val
if ( isset($_POST['Insert']) && db_dup_key($Tavola,$_POST) > 0 )  {
  $c_err->add("carrello Gi&agrave; Inserito","CODICE");
}

if (!$c_err->is_errore()) {
 if ( isset($_POST['Insert'])) {
   db_insert($Tavola,$_POST);

 }  else {
   db_update($Tavola,$_POST['ID'],$_POST);
 }
 header('Location: vis_carrello.php');
 exit;
}
}

// torno indietro
$indietro = "vis_carrello.php";
if (isset($_POST['Return'])) {
 header("Location: ".$indietro);
 exit;
}

require '../../Librerie/ges_html_top.php';

$c_err->mostra();
editor_js();
?>


<script type="text/javascript">

 var validator;
 $().ready(function($) {

   validator = $("#formG").validate({
    ignore : [],
    submitHandler: function(form) {
      form.submit();
    } ,
    rules: {
      NOME: {required: true},
      COGNOME: {required: true},
      EMAIL: { required: true, email:true, },
      ACQUISTATO: {required: true},
   }
 });

 });

</script>


<form id="formG" action="" method="post">

  <div id="generale">             
   <table width="100%" border=0>

    <input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >

    <tr>
      <td class="label" width="25%"> Nome: </td>
      <td width="75%"> 
      <input type="text" <?php $c_err->tooltip("NOME");?> name="NOME" id="NOME" value="<?php  if (isset($cur_rec['NOME'])) echo $cur_rec['NOME']; ?>"   size="15"  maxlength="500" >
      </td>                
    </tr>

    <tr>
      <td class="label" width="25%"> Cognome: </td>
      <td width="75%"> 
      <input type="text" <?php $c_err->tooltip("COGNOME");?> name="COGNOME" id="COGNOME" value="<?php  if (isset($cur_rec['COGNOME'])) echo $cur_rec['COGNOME']; ?>"   size="15"  maxlength="500" >
      </td>                
    </tr>  

  <tr>
    <td class="Label" width="5%" > Email: </td>
    <td width="65%">
      <input type="text" <?php $c_err->tooltip("EMAIL");?> name="EMAIL" id="EMAIL" value="<?php  if (isset($cur_rec['EMAIL'])) echo $cur_rec['EMAIL']; ?>"   size="15"  >
    </td>
  </tr>

  <tr>
    <td class="Label" width="5%" > Note: </td>
    <td width="65%">
      <textarea name="NOTE" id="NOTE" ><?php if (isset($cur_rec['NOTE'])) echo $cur_rec['NOTE']; ?>  </textarea>
    </td>
  </tr>

  <tr>
  <td class="label" width="25%"> Acquistato: </td>
  <td width="75%"> 
    <select name="ACQUISTATO"/>
      <?php 
        $val = "";
        if (isset($cur_rec['ACQUISTATO'])) $val = $cur_rec['ACQUISTATO'];
        if ($val == 0) {
          echo "
          <option value=\"0\" selected>No</option>
          <option value=\"1\">S&igrave;</option>
          ";
        } else {
          echo "
          <option value=\"0\" >No</option>
          <option value=\"1\" selected>S&igrave;</option>
          ";
        }
      ?>
  </select>
  </td>
  </tr>
</table>
</div>
               

                <script>
                  $(function() {
                    $( "#tab" ).tabs();
                  });

                  $('#TITOLO').validate({
                   rules: {
                     field: {
                       maxlength: 200
                     }
                   }
                  });
                </script>


                <center style="margin-top:20px;">
                 <button class="cancel" type="submit" name="Return" value="Return">Indietro</button>
                 <button type="submit" name="<?php echo $Funzione ?>" value="Salva">Salva</button>
               </center>


             </form>



             <?php require '../../Librerie/ges_html_bot.php';


             ?>
