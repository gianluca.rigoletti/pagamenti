<?php

require '../../Librerie/connect.php';
require '../../Librerie/html.php';
require '../../Librerie/files.php';
require '../../Librerie/i18n.php';    
require '../../Librerie/configurazione.php';

$c_files = new files();   

$Tavola= "pagamenti";


if ($_GET['p_upd']==1) {
 $Funzione = "Update";
 $Disabilita_chiave = "disabled";
 $Titolo = "Modifica Pagamento";
} else {
 $Funzione = "Insert";
 $Disabilita_chiave = "";
 $Titolo = "Nuovo Pagamento";
}

// se richiamato in update allora devo popolare il form
if ($_GET['p_upd']==1) {
 $risultato = db_query_mod($Tavola,$_GET['p_id']);
 $cur_rec = mysql_fetch_assoc($risultato);
 //$cur_rec['SCADENZA_VIS'] = db_converti_data($cur_rec['SCADENZA']);
}

// confermo
if ( isset($_POST['Insert']) || isset($_POST['Update']) ) {

 $cur_rec['ID'] = $_POST['ID'];
 $cur_rec['PRODOTTO'] = $_POST['PRODOTTO'];
 $cur_rec['DESCRIZIONE'] = $_POST['DESCRIZIONE'];
 $cur_rec['PREZZO'] = $_POST['PREZZO'];
 $cur_rec['SPESE_SPEDIZIONE'] = $_POST['SPESE_SPEDIZIONE'];
 $cur_rec['VISIBILITA'] = $_POST['VISIBILITA'];

if ($_POST['PRODOTTO'] == null || $_POST['PRODOTTO'] == " ") {
 $c_err->add("Campo PRODOTTO Obbligatorio","PRODOTTO");
}

if ($_POST['PREZZO'] == null || $_POST['PREZZO'] == " ") {
  $c_err->add("Campo PREZZO Obbligatorio","PREZZO");
}

if ($_POST['DESCRIZIONE'] == null || $_POST['DESCRIZIONE'] == " ") {
  $c_err->add("Campo DESCRIZIONE Obbligatorio","DESCRIZIONE");
} 

if ( $_POST['SPESE_SPEDIZIONE'] == null || $_POST['SPESE_SPEDIZIONE'] == " ") {
  $c_err->add("Campo Descrizione estesa Obbligatorio","SPESE_SPEDIZIONE");
}

if ($_POST['VISIBILITA'] == null || $_POST['VISIBILITA'] == " ") {
  $c_err->add("Campo Descrizione Iscrizione Obbligatorio","VISIBILITA");
} 



   // controllo dup-Val
if ( isset($_POST['Insert']) && db_dup_key($Tavola,$_POST) > 0 )  {
  $c_err->add("pagamenti Gi&agrave; Inserito","CODICE");
}

if (!$c_err->is_errore()) {
 if ( isset($_POST['Insert'])) {
   db_insert($Tavola,$_POST);

 }  else {
   db_update($Tavola,$_POST['ID'],$_POST);
 }
 header('Location: vis_pagamenti.php');
 exit;
}
}

// torno indietro
$indietro = "vis_pagamenti.php";
if (isset($_POST['Return'])) {
 header("Location: ".$indietro);
 exit;
}

require '../../Librerie/ges_html_top.php';

$c_err->mostra();
editor_js();
?>


<script type="text/javascript">

 var validator;
 $().ready(function($) {

   validator = $("#formG").validate({
    ignore : [],
    submitHandler: function(form) {
      form.submit();
    } ,
    rules: {
   	DESCRIZIONE: {required: true},
   	PREZZO: {
      required: true, 
      importoeuro : true,
    },
    SPESE_SPEDIZIONE: {required: true},
    PRODOTTO: {required: true},
    VISIBILITA: {required: true},
   }
 });

 });

</script>


<form id="formG" action="" method="post">

  <div id="generale">             
   <table width="100%" border=0>

    <input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >

    <tr>
      <td class="label" width="25%"> Prodotto: </td>
      <td width="75%"> 
      <input type="text" <?php $c_err->tooltip("PRODOTTO");?> name="PRODOTTO" id="PRODOTTO" value="<?php  if (isset($cur_rec['PRODOTTO'])) echo $cur_rec['PRODOTTO']; ?>"   size="15"  >
      </td>                
    </tr>

    <tr>
      <td class="label" width="25%"> Descrizione: </td>
      <td width="75%"> 
      <input type="text" <?php $c_err->tooltip("DESCRIZIONE");?> name="DESCRIZIONE" id="DESCRIZIONE" value="<?php  if (isset($cur_rec['DESCRIZIONE'])) echo $cur_rec['DESCRIZIONE']; ?>"   size="15"  >
      </td>                
    </tr>  

  <tr>
    <td class="Label" width="5%" > Prezzo: </td>
    <td width="65%">
      <input type="text" <?php $c_err->tooltip("PREZZO");?> name="PREZZO" id="PREZZO" value="<?php  if (isset($cur_rec['PREZZO'])) echo db_visimporti($cur_rec['PREZZO']); ?>"   size="15"  >
    </td>
  </tr>

  <tr>
  <td class="label" width="25%"> Spese di spedizione: </td>
  <td width="75%"> 
    <select name="SPESE_SPEDIZIONE"/>
      <?php 
        $val = "";
        if (isset($cur_rec['SPESE_SPEDIZIONE'])) $val = $cur_rec['SPESE_SPEDIZIONE'];
        if ($val == 0) {
          echo "
          <option value=\"0\" selected>No</option>
          <option value=\"1\">S&igrave;</option>
          ";
        } else {
          echo "
          <option value=\"0\" >No</option>
          <option value=\"1\" selected>S&igrave;</option>
          ";
        }
      ?>
  </select>
  </td>
  </tr>

  <tr>
  <td class="label" width="25%"> Visibilit&agrave; </td>
  <td width="75%"> 
    <select name="VISIBILITA"/>
      <?php 
        $val = "";
        if (isset($cur_rec['VISIBILITA'])) $val = $cur_rec['VISIBILITA'];
        if ($val == 0) {
          echo "
          <option value=\"0\" selected>No</option>
          <option value=\"1\">S&igrave;</option>
          ";
        } else {
          echo "
          <option value=\"0\" >No</option>
          <option value=\"1\" selected>S&igrave;</option>
          ";
        }
      ?>
  </select>
  </td>
  </tr>

 <?php                                 

  echo  "</table></div>"; 

                    ?>              
               

                <script>
                  $(function() {
                    $( "#tab" ).tabs();
                  });

                  $('#TITOLO').validate({
                   rules: {
                     field: {
                       maxlength: 200
                     }
                   }
                  });
                </script>


                <center style="margin-top:20px;">
                 <button class="cancel" type="submit" name="Return" value="Return">Indietro</button>
                 <button type="submit" name="<?php echo $Funzione ?>" value="Salva">Salva</button>
               </center>


             </form>



             <?php require '../../Librerie/ges_html_bot.php';


             ?>
