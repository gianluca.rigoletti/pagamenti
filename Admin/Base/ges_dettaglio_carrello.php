<?php

require '../../Librerie/connect.php';
require '../../Librerie/html.php';
require '../../Librerie/files.php';
require '../../Librerie/i18n.php';    
require '../../Librerie/configurazione.php';

$Tavola= "dettaglio_carrello";

if ($_GET['p_upd']==1) {
 $Funzione = "Update";
 $Disabilita_chiave = "disabled";
 $Titolo = "Modifica Dettaglio Carrello";
} else {
 $Funzione = "Insert";
 $Disabilita_chiave = "";
 $Titolo = "Nuovo Dettaglio Carrello";
}

// se richiamato in update allora devo popolare il form
if ($_GET['p_upd']==1) {
 $risultato = db_query_mod($Tavola,$_GET['p_id']);
 $cur_rec = mysql_fetch_assoc($risultato);
 $_GET['id'] = $cur_rec['IDCARRELLO'];
}

// confermo
if ( isset($_POST['Insert']) || isset($_POST['Update']) ) {

 $cur_rec['ID'] = $_POST['ID'];
 $cur_rec['IDCARRELLO'] = $_POST['IDCARRELLO'];
 $cur_rec['IDPAGAMENTI'] = $_POST['IDPAGAMENTI'];
 $cur_rec['DESCRIZIONE'] = $_POST['DESCRIZIONE'];
 $cur_rec['QTA'] = $_POST['QTA'];
 $cur_rec['PREZZO'] = $_POST['PREZZO'];
 $cur_rec['SITO'] = $_POST['SITO'];
 $cur_rec['SPESE_SPEDIZIONE'] = $_POST['SPESE_SPEDIZIONE'];

if ($_POST['IDCARRELLO'] == null || $_POST['IDCARRELLO'] == " ") {
 $c_err->add("Campo Corso Obbligatorio","IDCARRELLO");
}
if ($_POST['IDPAGAMENTI'] == null || $_POST['IDPAGAMENTI'] == " ") {
 $c_err->add("Campo IDPAGAMENTI Obbligatorio","IDPAGAMENTI");
}
if ($_POST['SITO'] == null || $_POST['SITO'] == " ") {
 $c_err->add("Campo Descrizione Orario Obbligatorio","SITO");
}
if ($_POST['QTA'] == null || $_POST['QTA'] == " ") {
 $c_err->add("Campo Descrizione Estesa Obbligatorio","QTA");
}
if ($_POST['SPESE_SPEDIZIONE'] == null || $_POST['SPESE_SPEDIZIONE'] == " ") {
 $c_err->add("Campo Descrizione Estesa Obbligatorio","SPESE_SPEDIZIONE");
}

   // controllo dup-Val
if ( isset($_POST['Insert']) && db_dup_key($Tavola,$_POST) > 0 )  {
  $c_err->add("Dettaglio Carrello Gi&agrave; Inserito","CODICE");
}


if (!$c_err->is_errore()) {
 if ( isset($_POST['Insert'])) {
   db_insert($Tavola,$_POST);

 }  else {
   db_update($Tavola,$_POST['ID'],$_POST);
 }
 header('Location: vis_dettaglio_carrello.php?id='.$_POST['IDCARRELLO']);
 exit;
}
}

// torno indietro
$indietro = "vis_dettaglio_carrello.php?id=".$_GET['id'];
if (isset($_POST['Return'])) {
 header("Location: ".$indietro);
 exit;
}

require '../../Librerie/ges_html_top.php';

$c_err->mostra();
editor_js();

$default_path = "../../config.json";

function getSito($value)
{
  
  global $default_path;
  $string = file_get_contents($default_path);
  $json = json_decode($string, true);
  if ($value == -1) {
    return $json['sito'];
  }
  $index = $value;
  return $json['sito'][$index];
}

function siNo($value)
{
  $bool = ($value == 0) ? "No" : "S&igrave;" ;
}

?>

<script type="text/javascript">

 var validator;
 $().ready(function($) {

  if ($('#IDPAGAMENTI').val() == "205" || $('#IDPAGAMENTI').val() == "") {
    $('#riga-prezzo').toggle();
    $('#riga-descrizione').toggle();
  };

  $('#IDPAGAMENTI').change(function() {
    if ($('#IDPAGAMENTI').val() == "205" || $('#IDPAGAMENTI').val() == "") {
      $('#riga-prezzo').toggle();
      $('#riga-descrizione').toggle();
    };
  });

    $('#<?php echo $Funzione; ?>').click(function() {
      var content = tinyMCE.activeEditor.getContent(); // get the content
      $('#QTA').val(content); // put it in the textarea
  });

   validator = $("#formG").validate({
    ignore : [],
    submitHandler: function(form) {
      form.submit();
    } ,
    rules: {
     IDCARRELLO: {required: true},
     IDPAGAMENTI: {required: true},
     PREZZO: { importoeuro: true },
     SITO: {required: true},
     SPESE_SPEDIZIONE: { required: true },
     QTA: {required: true, digits: true}
   }
 });

 });



</script>


<form id="formG" action="" method="post">
 <table width="100%" border=0>
   <tr><td class="px" height="10"></td></tr>
   <tr><td align="center">
     <table width="100%" border=0>

    <input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >

    <input type="hidden" name="IDCARRELLO" value="<?php if (isset($cur_rec)) echo $cur_rec['IDCARRELLO']; else echo $_GET['id']; ?>" >

    <tr>
      <td class="label" width="25%"> Quantit&agrave;: </td>
      <td width="75%"> 
      <input type="text" <?php $c_err->tooltip("QTA");?> name="QTA" id="QTA" value="<?php  if (isset($cur_rec['QTA'])) echo $cur_rec['QTA']; ?>"   size="15"  maxlength="500" >
      </td>                
    </tr>

    <tr id="riga-descrizione">
      <td class="Label" width="5%" > Descrizione: </td>
      <td width="65%">
        <textarea name="DESCRIZIONE" id="DESCRIZIONE" ><?php if (isset($cur_rec['DESCRIZIONE'])) echo $cur_rec['DESCRIZIONE']; ?>  </textarea>
      </td>
    </tr>


    <tr id="riga-prezzo">
      <td class="label" width="25%"> Prezzo: </td>
      <td width="75%"> 
      <input type="text" <?php $c_err->tooltip("PREZZO");?> name="PREZZO" id="PREZZO" value="<?php  if (isset($cur_rec['PREZZO'])) echo $cur_rec['PREZZO']; else echo "0"; ?>"   size="15"  maxlength="500">
      </td>                
    </tr>

    <tr>
      <td class="label" width="25%"> Sito: </td>
      <td width="75%"> 
      <select name="SITO">
        <?php 

          for ($i=0; $i < count(getSito(-1)); $i++) {
          $selected = ""; 
            if (isset($cur_rec['SITO']) && $cur_rec['SITO'] == $i) {
              $selected = "selected";
            }
            echo "<option value=\"".$i."\" ".$selected." >".getSito($i)."</option>";
          }
        ?>
      </select>
      </td>                
    </tr>

    <tr>
    <td class="label" width="25%"> Spese di spedizione: </td>
    <td width="75%"> 
      <select name="SPESE_SPEDIZIONE"/>
        <?php 
          $val = "";
          if (isset($cur_rec['SPESE_SPEDIZIONE'])) $val = $cur_rec['SPESE_SPEDIZIONE'];
          if ($val == 0) {
            echo "
            <option value=\"0\" selected>No</option>
            <option value=\"1\">S&igrave;</option>
            ";
          } else {
            echo "
            <option value=\"0\" >No</option>
            <option value=\"1\" selected>S&igrave;</option>
            ";
          }
        ?>
    </select>
    </td>
    </tr>

    <tr>
    <td class="Label" width="25%"> Tipo di pagamento: </td>
    <td width="75%"> 
        <select name="IDPAGAMENTI" id="IDPAGAMENTI"/>
          <?php if (isset($cur_rec)) {db_html_select_cod('pagamenti', $cur_rec['IDPAGAMENTI'],'ID','PRODOTTO',true,null);
          } 
                else {db_html_select_cod('pagamenti', '','ID','PRODOTTO',true,null);
          }  
    ?>

        </select>
    </td>                
    </tr>

  </table>
</div>   

<script>
  $(function() {
    $( "#tab" ).tabs();
  });

</script>

<center style="margin-top:20px;">
 <button class="cancel" type="submit" name="Return" value="Return">Indietro</button>
 <button type="submit" name="<?php echo $Funzione ?>" value="Salva">Salva</button>
</center>

</form>

<?php require '../../Librerie/ges_html_bot.php'; ?>
