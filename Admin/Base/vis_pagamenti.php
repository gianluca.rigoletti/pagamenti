<?php

require '../../Librerie/connect.php';

//if (!isset($_POST['Sito']))  $_POST['Sito'] = "";


$Titolo = "Gestione Pagamenti";
$Tavola= "pagamenti";

//*******************************************************
//FILTRI

$where = "";
$and = "";

if (isset($_POST['descrizione'])) {
      $where .= "upper(DESCRIZIONE) like upper('".str_replace("'","\'",str_replace("*","",$_POST['descrizione']))."%') ";
      $and = " and ";
      $where .= $and;

}

if (isset($_POST['prodotto'])) {
  $where .= "upper(PRODOTTO) like upper('".str_replace("'","\'",str_replace("*","",$_POST['prodotto']))."%') ";
  $and = " and ";
}

if (db_is_null($where)) $where = " 1 = 1";


//*****************************************************************

 $risultato = db_query_generale($Tavola,$where,"id");

require '../../Librerie/ges_html_top.php';
require '../../Librerie/html.php';

function siNo($value)
{
  $var = ($value == 0) ? "No" : "S&igrave;" ;
  return $var;
}

?>

<script>
  $(function() {
    $( "#descrizione" ).autocomplete({
      source: "autocomplete.php?tavola=pagamenti&campo=DESCRIZIONE" //creo un fil.php dove li passo pr ogni tavola
    });
    $( "#prodotto" ).autocomplete({
      source: "autocomplete.php?tavola=pagamenti&campo=PRODOTTO" //creo un fil.php dove li passo pr ogni tavola
    });

  });

  function conferma(a) {
     if (a.value != null && a.value.length != 0 ) {
        a.form.submit();
     }
  }

 </script>

       <form action="" method="post">
       <table>
            <tr><td colspan="8" class="px" height="30"></td></tr>

            <tr>

            <td class="Label"> Descrizione</td>
            <td >
                <input type="text" <?php $c_err->tooltip("descrizione");?>  value="<?php  /*if (isset($_POST['descrizione'])) echo $_POST['descrizione'];*/ ?>" name="descrizione" id="descrizione" size="20" />
            </td>

            <td class="Label"> Prodotto</td>
            <td >
                <input type="text" <?php $c_err->tooltip("prodotto");?>  value="<?php  /*if (isset($_POST['descrizione'])) echo $_POST['descrizione'];*/ ?>" name="prodotto" id="prodotto" size="20" />
            </td>

            <td>
               <input type="submit" name="VIS" value="Visualizza" onchange="this.form.submit()">
            </td>
            </tr>

            <tr><td colspan="8" class="px" height="10"></td></tr>
            <tr><td valign="middle" width="16px ">


            <a href="ges_pagamenti.php?p_upd=0"><img src="../../Icons/add.png"> </a>
            </td>
            <td valign="middle"><a href="ges_pagamenti.php?p_upd=0">Inserisci Nuovo Pagamento</a> </td>
            </tr>
            <tr><td valign="middle" width="16px ">


            <a href="ges_pagamenti_provvisori.php?p_upd=0"><img src="../../Icons/add.png"> </a>
            </td>
            <td valign="middle"><a href="ges_pagamenti_provvisori.php?p_upd=0">Inserisci Nuovo Pagamento Provvisiorio</a> </td>
            </tr>

            <tr><td colspan="8" class="px" height="20"></td></tr>

       </table>
       </form>

           <table width="100%" class="display" id="tabellavis">
           <thead>
            <tr>
            <th width="30%"> Prodotto </td>
            <th  width="40%"> Descrizione </td>
            <th  width="40%"> Prezzo </td>
            <th  width="40%"> Spese spedizione </td>
            <th  width="40%"> Visibilit&grave; </td>
            <th  width="5%"> &nbsp;</td>
            <th  width="5%"> &nbsp;</td>
            </tr>
            </thead>
            <tbody>
            <?php

            while ($cur_rec = mysql_fetch_assoc($risultato))
            {

                 echo "<tr>
                        <td >".$cur_rec['PRODOTTO']."   </td>
                        <td >".$cur_rec['DESCRIZIONE']."   </td>
                        <td >".db_visimporti($cur_rec['PREZZO'])."   </td>
                        <td >".siNo($cur_rec['SPESE_SPEDIZIONE'])."   </td>
                        <td >".siNo($cur_rec['VISIBILITA'])."   </td>

                        ";
                 echo "</td>
                 <td ><a href=\"ges_pagamenti.php?p_upd=1&p_id=".$cur_rec['ID']."\"><img class=\"link\" src=\"../../Icons/edit.gif\" title=\"modifica la riga\" /></a></td>
                        <td ><a href=\"Javascript:ut_delete_rec(".$cur_rec['ID'].",'del_pagamenti')\"><img class=\"link\" src=\"../../Icons/elimina.jpg\" title=\"cancella la riga\"/></a></td>
                    </tr>";

            }
            ?>




            </tbody>
        </table>

                    <div id="cancella" title="Cancella Corso">
               </div>

            <script type="text/javascript" charset="utf-8">
              $(document).ready(function() {
                $('#tabellavis').dataTable({
                "bJQueryUI": true,
            "sPaginationType": "full_numbers",
                "bFilter":true,
                "aoColumns": [
                            null,
                            null,
                            null,
                            { "bSortable": false },
                            { "bSortable": false },
                            { "bSortable": false },
                            { "bSortable": false }

                        ]

                 });


               $("#dettaglio").dialog({
                 autoOpen: false,
                 height: 'auto',
                 minHeight: 180,
                 maxHeight: 600,
                 width : 300,
                 minWidth: 300,
                 maxWidth: 300,
                 modal: true,
                 buttons: {
                  Ok: function() {
                    $( this ).dialog( "close" );
                  }}
                 } );



              } );

              function dett(id) {
                 var htmlStr = $('#dettaglio'+id).html();
                 $("#dettaglio").html(htmlStr);
                 $("#dettaglio").dialog("open");
              }


            </script>

                    <tr><td class="px" height="20"></td></tr>



                    <table >
                    <tr><td class="px" height="100"></td></tr>
                    </table>
                </td></tr></table>



        <?php require '../../Librerie/ges_html_bot.php'; ?>
