<?php

require '../../Librerie/connect.php';
require '../../Librerie/html.php';
require '../../Librerie/files.php';
require '../../Librerie/i18n.php';
require '../../Librerie/configurazione.php';

$Titolo = "Nuovo Pagamento Provvisorio";
$default_path = "../../config.json";

function getSito($value)
{

  global $default_path;
  $string = file_get_contents($default_path);
  $json = json_decode($string, true);
  if ($value == -1) {
    return $json['sito'];
  }
  $index = $value;
  return $json['sito'][$index];
}

function getUrl($value)
{

  global $default_path;
  $string = file_get_contents($default_path);
  $json = json_decode($string, true);
  if ($value == -1) {
    return $json['url'];
  }
  $index = $value;
  return $json['url'];
}

// torno indietro
$indietro = "vis_pagamenti.php";
if (isset($_POST['Return'])) {
 header("Location: ".$indietro);
 exit;
}

function generaLink($descrizione, $prezzo, $qta, $sito) {
  $string = '?id=0&qta='.$qta.'&description='.base64_encode($descrizione).'&site='.$sito.'&price='.base64_encode($prezzo).'&Input=Acquista';
  return $string;
}

if (isset($_POST['Salva']) && $_POST['Salva'] === 'Salva') {
  $link = generaLink($_POST['DESCRIZIONE'], $_POST['PREZZO'], $_POST['QUANTITA'], $_POST['SITO']);
  die(getUrl().$link);
}

require '../../Librerie/ges_html_top.php';
editor_js();
?>


<script type="text/javascript">

 var validator;
 $().ready(function($) {

   validator = $("#formG").validate({
    ignore : [],
    submitHandler: function(form) {
      form.submit();
    } ,
    rules: {
   	DESCRIZIONE: {required: true},
   	PREZZO: {
      required: true,
      importoeuro : true,
    },
    QUANTITA: {
      required: true,
      digits: true,
    },
    SITO: {
      required: true,
    }

   }
 });

 });

</script>


<form id="formG" action="" method="post">

  <div id="generale">
   <table width="100%" border=0>

    <tr>
      <td class="label" width="25%"> Descrizione: </td>
      <td width="75%">
      <textarea name="DESCRIZIONE" id="DESCRIZIONE" rows="8" cols="40"></textarea>
      </td>
    </tr>

  <tr>
    <td class="Label" width="5%" > Prezzo: </td>
    <td width="65%">
      <input type="text" name="PREZZO" id="PREZZO" size="15"  >
    </td>
  </tr>

  <tr>
    <td class="Label" width="5%" > Quantit&agrave;: </td>
    <td width="65%">
      <input type="number" name="QUANTITA" id="QUANTITA"  size="15"  >
    </td>
  </tr>

  <tr>
    <td class="label" width="25%"> Sito: </td>
    <td width="75%">
    <select name="SITO">
      <?php

        for ($i=0; $i < count(getSito(-1)); $i++) {
        $selected = "";
          if (isset($cur_rec['SITO']) && $cur_rec['SITO'] == $i) {
            $selected = "selected";
          }
          echo "<option value=\"".$i."\" ".$selected." >".getSito($i)."</option>";
        }
      ?>
    </select>
    </td>
  </tr>

 <?php

  echo  "</table></div>";

                    ?>


                <script>
                  $(function() {
                    $( "#tab" ).tabs();
                  });

                  $('#TITOLO').validate({
                   rules: {
                     field: {
                       maxlength: 200
                     }
                   }
                  });
                </script>


                <center style="margin-top:20px;">
                 <button class="cancel" type="submit" name="Return" value="Return">Indietro</button>
                 <button type="submit" name="Salva" value="Salva">Genera Link</button>
               </center>


             </form>



             <?php require '../../Librerie/ges_html_bot.php';


             ?>
