-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Creato il: Gen 25, 2016 alle 03:15
-- Versione del server: 5.6.27
-- Versione PHP: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pagamenti`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `amministratori`
--

CREATE TABLE `amministratori` (
  `ID` int(10) UNSIGNED NOT NULL,
  `User` varchar(45) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Pword` varchar(45) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Livello` int(10) UNSIGNED DEFAULT NULL COMMENT '1=superadmin; 2=adminrwanda; 3=adminsito',
  `LastLogin` datetime DEFAULT NULL,
  `IPLastLogin` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Mail` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Tel` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Nome` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Cognome` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Cod` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IDCod` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IDSito` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `amministratori`
--

INSERT INTO `amministratori` (`ID`, `User`, `Pword`, `Livello`, `LastLogin`, `IPLastLogin`, `Mail`, `Tel`, `Nome`, `Cognome`, `Cod`, `IDCod`, `IDSito`) VALUES
(2, 'azordan', 'orione2k', 1, '2011-08-11 11:19:32', '82.60.83.86', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(1, 'corrado', 'master1122', 1, '2011-07-18 17:51:38', '93.66.99.215', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(17, 'd', 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, 'fausto', 'freddi', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE `carrello` (
  `ID` int(11) NOT NULL,
  `NOME` varchar(500) DEFAULT NULL,
  `COGNOME` varchar(500) DEFAULT NULL,
  `EMAIL` varchar(500) DEFAULT NULL,
  `NOTE` text,
  `ACQUISTATO` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `dettaglio_carrello`
--

CREATE TABLE `dettaglio_carrello` (
  `ID` int(11) NOT NULL,
  `IDCARRELLO` int(11) NOT NULL,
  `IDPAGAMENTI` int(11) NOT NULL,
  `QTA` int(11) NOT NULL,
  `SITO` tinyint(4) NOT NULL,
  `DESCRIZIONE` text NOT NULL,
  `PREZZO` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `pagamenti`
--

CREATE TABLE `pagamenti` (
  `ID` int(15) NOT NULL,
  `PRODOTTO` varchar(500) DEFAULT NULL,
  `DESCRIZIONE` varchar(500) DEFAULT NULL,
  `PREZZO` decimal(10,2) DEFAULT NULL,
  `SPESE_SPEDIZIONE` int(1) DEFAULT '0',
  `VISIBILITA` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagamenti`
--

INSERT INTO `pagamenti` (`ID`, `PRODOTTO`, `DESCRIZIONE`, `PREZZO`, `SPESE_SPEDIZIONE`, `VISIBILITA`) VALUES
(1, 'Iscrizione Esame i-TEST Self Assessment ', '', '28.00', 0, 1),
(2, 'Iscrizione Esame i-TEST Certificate ', '', '44.00', 0, 1),
(3, 'Corso TOEFL ', '', '695.00', 0, 1),
(4, 'Corso GMAT ', '', '1330.00', 0, 1),
(5, 'TOEFL Practice Online -1 test ', 'Simulazione test toefl ', '55.00', 0, 1),
(6, 'Supplemento Urgenza TOEFL ', '', '35.00', 0, 1),
(7, 'Iscrizione Esame GRE ', '', '195.00', 0, 1),
(8, 'B1, B2, F, M, J, C1D - Visa Processing Fee ', 'B1, B2, F, M, J, C1D - Visa Processing Fee ', '370.00', 0, 1),
(9, 'E- Visa Processing Fee ', '', '850.00', 0, 1),
(10, 'The Official Guide to the New TOEFL test 4th Edit. ', '', '40.00', 1, 1),
(11, 'GMAT Review ', '', '55.00', 1, 1),
(12, 'The Official Guide to the GRE revised General test 2nd edition ', '', '30.00', 1, 1),
(13, 'The Official SAT Study Guide 2nd edition ', '', '30.00', 1, 1),
(14, 'Mappa USA ', '', '15.00', 0, 1),
(15, 'Application Essay Package ', 'Application Essay Assistance ', '400.00', 0, 1),
(16, 'Application Essay Assistance ', 'Application Essay Assistance ', '200.00', 0, 1),
(17, 'Application Essay Assistance ', 'Discount ', '75.00', 0, 1),
(18, 'Private Lesson ', 'GMAT 1h 45 module ', '250.00', 0, 1),
(19, 'Private Lesson ', 'GMAT discount / other ', '200.00', 0, 1),
(20, 'Private Lesson ', 'non-GMAT discount ', '150.00', 0, 1),
(21, 'Traduzioni ', 'Traduzioni Legalizzate ', '246.12', 1, 1),
(22, 'SEVIS Fee J Visa ', 'Pagamento Sevis ', '200.00', 0, 1),
(23, 'Application Essay Assistance ', 'Application Essay Assistance ', '187.50', 0, 1),
(24, 'Shipment Fees ', '', '40.00', 0, 1),
(25, 'GMAT 2nd course discount ', 'GMAT 2nd course discount ', '1255.00', 0, 1),
(26, 'TOEFL 2nd course discount ', 'TOEFL 2nd course discount ', '620.00', 0, 1),
(27, 'TOEFL course book discount ', 'TOEFL course book discount ', '655.00', 0, 1),
(28, 'GMAT Official Guide (no shipping charge) ', 'GMAT Official Guide (vendita diretta) ', '55.00', 0, 1),
(29, 'TOEFL Official Guide (no shipping charge) ', 'TOEFL Official Guide (vendita diretta) ', '45.00', 0, 1),
(30, 'Cambio data TOEFL ', 'Cambio data TOEFL ', '60.00', 0, 1),
(31, 'Iscrizione TOEFL ', '', '230.00', 0, 1),
(32, 'Additional Score Request ', 'TOEFL ', '15.00', 0, 1),
(33, 'i-TEST Programma Scuole ', '', '12.00', 0, 1),
(34, 'Lista Nozze - 50 miglia ', 'Lista Nozze - 50 miglia ', '50.00', 0, 1),
(35, 'Integrazione Essay ', 'Integrazione Essay ', '100.00', 0, 1),
(36, 'Lista Nozze - 150 miglia ', 'Lista Nozze - 150 miglia ', '150.00', 0, 1),
(37, 'Lista Nozze Trussoni ', 'Lista Nozze Trussoni ', '200.00', 0, 1),
(38, 'Lista Nozze - 300 miglia ', 'Lista Nozze - 300 miglia ', '300.00', 0, 1),
(39, 'Lista Nozze - 400 miglia ', 'Lista Nozze - 400 miglia ', '400.00', 0, 1),
(40, 'GMAT course - discount ', 'GMAT course - discount ', '740.00', 0, 1),
(41, 'TOEIC Simulator ', 'TOEIC Simulator ', '35.00', 0, 1),
(42, 'spese cambio volo ', 'spese cambio volo ', '260.00', 0, 1),
(43, 'Emissione biglietto aereo ', 'Clemente David ', '433.00', 0, 1),
(44, 'GRE Reschedule ', 'GRE Reschedule ', '55.00', 0, 1),
(45, 'ESTA Servizio Richiesta Autorizzazione VWP ', 'ESTA ', '30.00', 0, 1),
(46, 'Traduzioni Certificati ', 'Traduzioni Certificati ', '136.64', 0, 1),
(47, 'Philadelphia e la regione dei Mormoni ', 'Escursioni ', '140.00', 0, 1),
(48, 'Washington D.C. ', 'Escursioni ', '140.00', 0, 1),
(49, 'Washington DC - Doppia ', 'Escursioni ', '261.00', 0, 1),
(50, 'Washington DC - Singola ', 'Escursioni ', '315.00', 0, 1),
(51, 'Boston, MA ', 'Escursioni ', '140.00', 0, 1),
(52, 'Niagara Falls & Shop - Doppia ', 'Escursioni ', '261.00', 0, 1),
(53, 'Niagara Falls & Shop - Singola ', 'Escursioni ', '315.00', 0, 1),
(54, 'SEVIS Fee - F Visa ', 'SEVIS Fee - F Visa ', '200.00', 0, 1),
(55, 'Esame TOEIC con EXPRESS SCORE ', 'Esame TOEIC con EXPRESS SCORE ', '107.00', 0, 1),
(56, 'Lista Nozze - 500 miglia ', 'Lista Nozze - 500 miglia ', '500.00', 0, 1),
(57, 'Lista Nozze - 1000 miglia ', 'Lista Nozze - 1000 miglia ', '1000.00', 0, 1),
(58, 'Lista Nozze - 1500 miglia ', 'Lista Nozze - 1500 miglia ', '1500.00', 0, 1),
(59, 'SARCINA x 2 ', 'Supplemento auto ', '116.00', 0, 1),
(60, 'acconto corso EAS ', 'acconto corso EAS ', '300.00', 0, 1),
(61, 'Lista Nozze Vertola-Della Bosca - 400 miglia ', 'Lista Nozze Vertola-Della Bosca - 400 miglia ', '400.00', 0, 1),
(62, 'Lista Nozze Vertola-Della Bosca - 500 miglia ', 'Lista Nozze Vertola-Della Bosca - 500 miglia ', '500.00', 0, 1),
(63, 'SEVIS Fee - J Visa ', 'SEVIS Fee - J Visa ', '180.00', 0, 1),
(64, 'Certificato Sotitutivo Provvisorio ', 'Certificato Sotitutivo Provvisorio ', '10.00', 0, 1),
(65, 'corso + hotel a manchester ', 'acconto corso + saldo hotel ', '792.00', 0, 1),
(66, 'Competenze ASC ', 'Competenze ASC ', '60.00', 0, 1),
(67, 'TOEFL iBT Workshop ', 'TOEFL iBT Workshop ', '85.00', 0, 1),
(68, 'iscrizione Toefl - Libro - Ups ', 'iscrizione Toefl - Libro - Ups ', '251.00', 0, 1),
(69, 'Lottery Application ', 'Lottery Application ', '80.00', 0, 1),
(70, 'Iscrizione Esame TOEIC ', 'Iscrizione Esame TOEIC ', '90.00', 0, 1),
(71, 'Diritti di agenzia su biglietteria aerea ', 'Diritti di agenzia su biglietteria aerea ', '40.00', 0, 1),
(72, 'TEST TOEIC Prima Volta presso EAS ', 'TEST TOEIC Prima Volta presso EAS ', '90.00', 0, 1),
(73, 'TEST TOEIC Sessioni Successive presso EAS ', 'TEST TOEIC Sessioni Successive presso EAS ', '80.00', 0, 1),
(74, 'EXPRESS SCORE ', 'EXPRESS SCORE ', '17.00', 0, 1),
(75, 'CERTIFICATO Richiesto Prima del Test ', 'CERTIFICATO Richiesto Prima del Test ', '15.00', 0, 1),
(76, 'CERTIFICATO Richiesto Dopo l''effettuazione Test ', 'CERTIFICATO Richiesto Dopo l''effettuazione Test ', '25.00', 0, 1),
(77, 'TEST TOEIC Public Session ', 'TEST TOEIC Public Session ', '130.00', 0, 1),
(78, 'SAT Course ', 'SAT Course ', '895.00', 0, 1),
(79, 'Solo Appuntamento Consolato ', 'Solo Appuntamento Consolato ', '20.00', 0, 1),
(80, 'Fee per Atto Notarile ', 'Fee per Atto Notarile ', '20.00', 0, 1),
(81, 'TOEIC Simulazione Singola ', 'TOEIC Simulazione Singola ', '10.00', 0, 1),
(82, 'LIBRO PREPARAZIONE TOEIC ', 'LIBRO PREPARAZIONE TOEIC ', '48.00', 1, 1),
(83, 'Escursione a Washington ', 'Escursione a Washington ', '300.00', 0, 1),
(84, 'Lezione Privata General English ', 'Lezione Privata General English ', '48.00', 0, 1),
(85, 'TOEIC Test EAS - Mantova ', 'TOEIC Test EAS - Mantova ', '132.00', 0, 1),
(86, 'Test TOEIC Sessioni successive presso EAS - MANTOVA ', 'Test TOEIC Sessioni successive presso EAS - MANTOVA ', '105.00', 0, 1),
(87, 'Lezione Privata Promo 2h. ', 'Lezione Privata Promo 2h. ', '108.00', 0, 1),
(88, 'penale cambio biglietto aereo ', 'penale cambio biglietto aereo ', '110.00', 0, 1),
(89, 'SAFILO ', 'SAFILO ', '1700.00', 0, 1),
(90, 'Lista Nozze Mottola-De Santo - 150 miglia ', 'Lista Nozze Mottola-De Santo - 150 miglia ', '150.00', 0, 1),
(91, 'Lista Nozze Mottola-De Santo - 200 miglia ', 'Lista Nozze Mottola-De Santo - 200 miglia ', '200.00', 0, 1),
(92, 'Lista Nozze Mottola-De Santo - 300 miglia ', 'Lista Nozze Mottola-De Santo - 300 miglia ', '300.00', 0, 1),
(93, 'Lista Nozze Mottola-De Santo - 400 miglia ', 'Lista Nozze Mottola-De Santo - 400 miglia ', '400.00', 0, 1),
(94, 'Cambio data ', 'cambio data ', '525.00', 0, 1),
(95, 'TOEIC Speaking & Writing Sessione Privata ', 'TOEIC Speaking & Writing Sessione Privata ', '110.00', 0, 1),
(96, 'Simulazione TOEIC S&W ', 'Simulazione TOEIC S&W ', '50.00', 0, 1),
(97, 'Test TOEIC prima volta presso EAS - Catanzaro ', 'Test TOEIC prima volta presso EAS - Catanzaro ', '115.00', 0, 1),
(98, 'Test TOEIC Sessioni successive presso EAS - Catanzaro ', 'Test TOEIC Sessioni successive presso EAS - Catanzaro ', '105.00', 0, 1),
(99, 'Simulazione TOEIC a Mantova ', 'Simulazione TOEIC a Mantova ', '10.00', 0, 1),
(100, 'integrazione quota iscrizione Toefl ', 'integrazione quota iscrizione Toefl ', '9.00', 0, 1),
(101, 'Simulazione TOEIC a Catanzaro ', 'Simulazione TOEIC a Catanzaro ', '10.00', 0, 1),
(102, 'Lezioni Private 1h.30 ', 'Lezioni Private 1h.30 ', '150.00', 0, 1),
(103, 'cambio data TOEIC ', 'cambio data TOEIC ', '20.00', 0, 1),
(104, 'Revisione Linguistica Essay/ora ', 'Revisione Linguistica Essay/ora ', '75.00', 0, 1),
(105, 'MAT - ALTIS Applicant ', 'MAT - ALTIS Applicant ', '30.00', 0, 1),
(106, 'TOEIC Lezione Privata ', 'TOEIC Lezione Privata ', '48.00', 0, 1),
(107, 'CERTIFICATO TOEIC Promozione ', 'CERTIFICATO TOEIC Promozione ', '20.00', 0, 1),
(108, 'Spedizione raccomandata ', 'spedizione raccomandata ', '5.00', 0, 1),
(109, 'H, L, O, P, Q, R - Visa Processing Fee ', 'H, L, O, P, Q, R - Visa Processing Fee ', '430.00', 0, 1),
(110, 'GRE Classes ', 'GRE Classes ', '575.00', 0, 1),
(111, 'Corso GMAT Ripetizione ', 'Corso GMAT Ripetizione ', '495.00', 0, 1),
(112, 'Corso SAT MAT II ', 'Corso SAT MAT II ', '375.00', 0, 1),
(113, 'Corso TOEFL ripetizione ', 'Corso TOEFL ripetizione ', '350.00', 0, 1),
(114, 'Fee consolari Visti E ', 'Fee consolari Visti E ', '250.00', 0, 1),
(115, 'Visto J1 + Tassa SEVIS ', 'Visto J1 + Tassa SEVIS ', '380.00', 0, 1),
(116, 'TOEIC Workshop ', 'TOEIC Workshop ', '75.00', 0, 1),
(117, 'Iscrizione GMAT Test ', 'Iscrizione GMAT Test ', '250.00', 0, 1),
(118, 'TOEFL Course - VAT exempt ', 'TOEFL Course - VAT exempt ', '495.83', 0, 1),
(119, 'Lezioni private Inglese 1.5 ore ', 'Lezioni private Inglese 1.5 ore ', '72.00', 0, 1),
(120, 'Saldo biglietteria ', 'Saldo biglietteria ', '473.00', 0, 1),
(121, 'Saldo Corso GMAT ', 'Saldo Corso GMAT ', '545.83', 0, 1),
(122, 'Acconto corso studio ', 'Acconto corso studio ', '300.00', 0, 1),
(123, 'visto F1 + tasse SEVIS ', 'visto F1 + tasse SEVIS ', '475.00', 0, 1),
(124, 'Esame TOEIC - OK LAVORO ', 'Esame TOEIC - OK LAVORO ', '115.00', 0, 1),
(125, 'Spese spedizione + Differenza tipo visto ', 'Spese spedizione + Differenza tipo visto ', '30.00', 0, 1),
(126, 'GRE - Special Offer - esami entro il 30/9/2011 ', 'GRE - Special Offer - esami entro il 30/9/2011 ', '95.00', 0, 1),
(127, 'Saldo biglietto ', 'Saldobiglietto ', '282.00', 0, 1),
(128, 'GMAT - no VAT ', 'GMAT - no VAT ', '739.67', 0, 1),
(129, 'Lista nozze Trussoni ', 'Lista nozze Trussoni ', '50.00', 0, 1),
(130, 'TOEIC ', 'TOEIC ', '77.00', 0, 1),
(131, 'Lezioni Inglese - Ms. Viglietti ', 'Lezioni Inglese - Ms. Viglietti ', '45.00', 0, 1),
(132, 'Test TOEIC prima volta presso EAS - Viterbo ', 'Test TOEIC prima volta presso EAS - Viterbo ', '115.00', 0, 1),
(133, 'Test TOEIC Sessioni successive presso EAS - Viterbo ', 'Test TOEIC Sessioni successive presso EAS - Viterbo ', '105.00', 0, 1),
(134, 'SAT Subject Test Math II ', 'SAT Subject Test Math II ', '425.00', 0, 1),
(135, 'Simulazione TOEIC a Viterbo ', 'Simulazione TOEIC a Viterbo ', '10.00', 0, 1),
(136, 'Duplicato Certificato ', 'Duplicato Certificato ', '30.00', 0, 1),
(137, 'Visa application - self filed DS160 ', 'Visa application - self filed DS160 ', '265.00', 0, 1),
(138, 'Lezioni Individuali + Libri ', 'Lezioni Individuali + Libri ', '660.00', 0, 1),
(139, 'Lezione SAT gruppo - 2 ore ', 'Lezione SAT gruppo - 2 ore ', '120.00', 0, 1),
(140, 'Ms. Manenti Cristina ', 'Ms. Manenti Cristina ', '854.00', 0, 1),
(141, 'Copie ESTA ', 'Copie ESTA ', '5.00', 0, 1),
(142, 'Pacchetto biglietti Roland Garros ', 'Pacchetto biglietti Roland Garros ', '2120.00', 0, 1),
(143, 'Spese spedizione Ms. Contu ', 'Spese spedizione Ms. Contu ', '26.00', 0, 1),
(144, 'Cambio Data Appuntamento Consolato ', 'Cambio Data Appuntamento Consolato ', '20.00', 0, 1),
(145, 'Business Workshop ', 'Business Workshop ', '149.00', 0, 1),
(146, 'Simulazione Singola Bari ', 'Simulazione Singola Bari ', '20.00', 0, 1),
(147, 'Visti USA ', 'Visti USA ', '275.00', 0, 1),
(148, 'Promozione Corso Inglese - 15 ore ', 'Promozione Corso Inglese - 15 ore ', '150.00', 0, 1),
(149, 'Discounted SAT Course ', 'Discounted SAT Course ', '820.00', 0, 1),
(150, 'Discounted English Class - - 1 hour ', 'Discounted English Class - 1 hour ', '36.30', 0, 1),
(151, 'Lezione Individuale scontata ', 'Lezione Individuale scontata ', '112.50', 0, 1),
(152, 'TOEIC Polimi 2° volta ', 'TOEIC Polimi 2° volta ', '97.00', 0, 1),
(153, 'Materiale GMAT ', 'Materiale GMAT ', '10.00', 0, 1),
(154, 'Lista Nozze - 600 miglia ', 'Lista Nozze - 600 miglia ', '600.00', 0, 1),
(155, 'Lista Nozze - 700 miglia ', 'Lista Nozze - 700 miglia ', '700.00', 0, 1),
(156, 'Lista Nozze - 800 miglia ', 'Lista Nozze - 800 miglia ', '800.00', 0, 1),
(157, 'Biglietteria aerea Miami/PBI/Nyc/Mil ', 'Saldo biglietteria aerea ', '4775.00', 0, 1),
(158, 'saldo biglietto ', 'saldo biglietto ', '261.78', 0, 1),
(159, 'Lez. Private SAT ', 'Lez. Private SAT ', '850.00', 0, 1),
(160, 'SAT package ', 'SAT package ', '650.00', 0, 1),
(161, 'Differrenza Visto E ', 'Differrenza Visto E ', '405.00', 0, 1),
(162, 'Compilazione Modulo x visto USA ', 'Compilazione Modulo x visto USA ', '75.00', 0, 1),
(163, 'Application for crew member ', 'Application for crew member ', '200.00', 0, 1),
(164, 'Iscrizione Test EMAT ', 'Iscrizione Test EMAT ', '50.00', 0, 1),
(165, 'TOEIC Speaking & Writing Sessione Pubblica ', 'TOEIC Speaking & Writing Sessione Pubblica ', '130.00', 0, 1),
(166, 'Corso GMAT sconto 2'' corso ', 'Corso GMAT sconto 2'' corso ', '645.00', 0, 1),
(167, 'MAT payment difference ', 'MAT payment difference ', '23.00', 0, 1),
(168, 'Application Fee Corsi ', 'Application Fee Corsi ', '75.00', 0, 1),
(169, 'TOEFL Classes ', 'TOEFL Classes ', '575.00', 0, 1),
(170, 'TOEIC on-line Prep Mod. 1,2,3 ', 'TOEIC on-line Prep Mod. 1,2,3 ', '70.00', 0, 1),
(171, 'Corso Inglese Modulo 21 ore ', 'Corso Inglese Modulo 21 ore ', '229.00', 0, 1),
(172, 'Workshop ', 'Workshop ', '149.00', 0, 1),
(173, 'Corso GMAT convenzione Politecnico ', 'Corso GMAT convenzione Politecnico ', '1159.00', 0, 1),
(174, 'Corso TOEFL Convenzione POLIMI ', 'Corso TOEFL Convenzione POLIMI ', '595.00', 0, 1),
(175, 'Lezioni Semi individuali TOEIC ', 'Lezioni Semi individuali TOEIC x 2 ', '30.00', 0, 1),
(176, 'The Official Guide to the New TOEFL IBT 3rd Edit. ', 'The Official Guide to the New TOEFL IBT 3rd Edit. ', '40.00', 0, 1),
(177, '5 Official TOEFL iBT Tests with Audio ', 'The Official Guide to the New TOEFL IBT - 5 Tests ', '40.00', 1, 1),
(178, 'Iscrizione SAT General Test ', 'Iscrizione SAT General Test ', '100.00', 0, 1),
(179, 'Spese visto J ', 'Spese visto J ', '195.00', 0, 1),
(180, 'Lezione da 2,5 ore corso GMAT ', 'Lezione da 2,5 ore corso GMAT ', '50.00', 0, 1),
(181, 'Iscrizione Test TOEFL Junior ', 'Iscrizione Test TOEFL Junior ', '75.00', 0, 1),
(182, 'Lezioni Inglese ', 'Lezioni Inglese ', '195.00', 0, 1),
(183, 'Lezioni Semi individuali ', 'Lezioni Semi individuali ', '125.00', 0, 1),
(184, 'Lezioni Semi individuali TOEIC ', 'Lezioni Semi individuali TOEIC x 3 ', '25.00', 0, 1),
(185, 'TOEIC L&R + S&W (Bundle) ', 'TOEIC L&R + S&W (Bundle) ', '190.00', 0, 1),
(186, 'Spese Spedizione Corriere ', 'Spese Spedizione Corriere ', '15.00', 0, 1),
(187, 'Correzione/traduzione Essay ', 'Correzione/traduzione Essay ', '187.50', 0, 1),
(188, 'Volo New York ', 'Volo New York ', '707.01', 0, 1),
(189, 'Additional Subject Test ', 'Additional Subject Test ', '50.00', 0, 1),
(190, 'Iscrizione SAT Subject ', 'Iscrizione SAT Subject ', '95.00', 0, 1),
(191, 'Lista Nozze Emanuele ', 'Lista Nozze Emanuele ', '200.00', 0, 1),
(192, 'Volo San Francisco ', 'Volo San Francisco ', '1378.83', 0, 1),
(193, 'Lezioni Inglese ', 'Lezioni Inglese ', '130.00', 0, 1),
(194, 'Assicurazione Globy ', 'Assicurazione Globy ', '139.00', 0, 1),
(195, 'Assicurazione Viaggio ', 'Assicurazione Viaggio ', '79.00', 0, 1),
(196, 'Visa Application - no Consular Fees ', 'Visa Application - no Consular Fees ', '147.00', 0, 1),
(197, 'Polizza Globy Rosso ', 'Polizza Globy Rosso ', '129.00', 0, 1),
(198, 'Simulazione TOEIC Speaking & Writing ', 'Simulazione TOEIC Speaking & Writing ', '55.00', 0, 1),
(199, 'Visa type: I ', 'Visa type: I ', '370.00', 0, 1),
(200, 'TOEIC on-line Prep Mod. 2,3 ', 'TOEIC on-line Prep Mod. 2,3 ', '60.00', 0, 1),
(201, 'Integrazione Pagamento ', 'Integrazione Pagamento ', '15.00', 0, 1),
(202, 'Tactics for TOEIC® Speaking and Writing Tests ', 'Tactics for TOEIC® Speaking and Writing Tests ', '48.00', 1, 1),
(203, 'Lezioni Private Inglese ', 'Lezioni Private Inglese ', '45.00', 0, 1),
(205, 'Nullo', NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `spedizioni`
--

CREATE TABLE `spedizioni` (
  `ID` int(11) NOT NULL,
  `IDCARRELLO` int(11) NOT NULL,
  `TIPO` int(11) DEFAULT NULL,
  `DESTINATARIO` varchar(500) DEFAULT NULL,
  `INDIRIZZO` varchar(500) DEFAULT NULL,
  `CAP` int(5) DEFAULT NULL,
  `CITTA` varchar(500) DEFAULT NULL,
  `NAZIONE` varchar(500) DEFAULT NULL,
  `STATO` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `amministratori`
--
ALTER TABLE `amministratori`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `dettaglio_carrello`
--
ALTER TABLE `dettaglio_carrello`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `pagamenti`
--
ALTER TABLE `pagamenti`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `spedizioni`
--
ALTER TABLE `spedizioni`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `amministratori`
--
ALTER TABLE `amministratori`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT per la tabella `carrello`
--
ALTER TABLE `carrello`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `dettaglio_carrello`
--
ALTER TABLE `dettaglio_carrello`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `pagamenti`
--
ALTER TABLE `pagamenti`
  MODIFY `ID` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;
--
-- AUTO_INCREMENT per la tabella `spedizioni`
--
ALTER TABLE `spedizioni`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
