<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/custom.css">
  </head>
  <body>

  <div class="container">
  	<div class="row">
  		<div class="row">
  			<div class="col-xs-12">
  				<img src="http://www.americanservice.eu/img/logo_store_1.gif" alt="" class="img-fluid pull-xs-left">
  				<img src="http://www.americanservice.eu/img/cart.gif" alt="" class="img-fluid pull-xs-right">
  			</div>
  		</div>
  		<hr>
  		<div class="row">
  			<div class="col-xs-12">
  				<nav class="nav nav-inline ">
  						<a href="" class="nav-link "><span class="h2">1</span> <span>Shopping Cart</span><hr></a>
  						<a href="" class="nav-link"><span class="h2">2</span> <span>Personal Data</span><hr></a>
  						<a href="" class="nav-link"><span class="h2">3</span> <span>Confirm Data</span><hr></a>
  						<a href="" class="nav-link active"><span class="h2">4</span> <span>Payment</span><hr></a>
  						<a href="" class="nav-link disabled"><span class="h2">5</span> <span>Result</span><hr></a>
  				</nav>
  			</div>
  		</div>
  		<div class="row ">
        <div class="vertical-spacing">

        </div>
        <h1>
          Il pagamento è andato a buon fine
        </h1>
        <div class="vertical-spacing">
        </div>
        <button class="btn btn-default btn-large">
          <a href="localhost:8080/cart.php">
            Torna indietro
          </a>
        </button>
      </div>
