<?php
require 'Librerie/html.php';
require 'Librerie/configurazione.php';
require 'ges_cart.php';

$carrello = new Cart($_SESSION['id_cart']);
if (!isset($_SESSION['payment']) ) {
    die(print_r($_SESSION));
    header("Location: cart.php");
}



  $merchantDomain = 'http://127.0.0.1:8080/pagamenti';
  $setefiPaymentGatewayDomain = 'test.monetaonline.it';
  $terminalId = '99999999';
  $terminalPassword = '99999999';


  $parameters = array(
	  'id'                    => $terminalId,
	  'password'              => $terminalPassword,
	  'operationType'         => 'initialize',
	  'amount'                => $carrello->getTotal(),
	  'currencyCode'          => '978',
	  'language'              => 'ITA',
	  'responseToMerchantUrl' => $merchantDomain.'/notify-setefi.php',
	  'recoveryUrl'           => $merchantDomain.'/result-fail.php',
	  'merchantOrderId'       => 'Payment'.$_SESSION['id_cart'],
    'cardHolderName'        => $carrello->getNome(),
    'cardHolderEmail'       => $carrello->getEmail(),
	  'description'           => 'Acquisto da Tecnitravel',
    'customField'           => $carrello->getNote()
  );
  /*
  $parameters = array(
	  'id' => $terminalId,
	  'password' => $terminalPassword,
	  'operationType' => 'initialize',
	  'amount' => '1.00',
	  'currencyCode' => '978',
	  'language' => 'ITA',
	  'responseToMerchantUrl' => $merchantDomain.'/do-notify-setefi.php',
	  'recoveryUrl' => $merchantDomain.'/do-pagamentoko.php',
	  'merchantOrderId' => 'TRCK0001',
    'cardHolderName' => 'Tom Smith',
    'cardHolderEmail'  => 'tom.smith@test.com',
	  'description' => 'Descrizione',
    'customField' => 'Custom Field'
  );*/

  //die(var_dump($parameters));

  //unset($_SESSION['PAGAMENTO']);

  $curlHandle = curl_init();
  curl_setopt($curlHandle, CURLOPT_URL, $setefiPaymentGatewayDomain.'/monetaweb/payment/2/xml');
  curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curlHandle, CURLOPT_POST, true);
  curl_setopt($curlHandle, CURLOPT_POSTFIELDS, http_build_query($parameters));
  //curl_setopt($curlHandle, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
  $xmlResponse = curl_exec($curlHandle);
  curl_close($curlHandle);

  $response = new SimpleXMLElement($xmlResponse);
  $paymentId = $response->paymentid;
  $paymentUrl = $response->hostedpageurl;

  $securityToken = $response->securitytoken;

  $setefiPaymentPageUrl = "$paymentUrl?PaymentID=$paymentId";
  header("Location: $setefiPaymentPageUrl");

?>
