<?php


class configurazione
{

var $valori = array();

  function lettura($val) {
  
      $risultato = db_query_fk_cod('configurazioni', $val, 'CODICE');
      $cur_rec = mysql_fetch_assoc($risultato);
      if (isset($cur_rec['VALORE']) ) return $cur_rec['VALORE'];
      
      return null;
  }

  function lung_max_art() {
      $lung = $this->lettura('LUNG_ARTICOLO_MAX');
      if ($lung == null || !db_isnumeric($lung) || !db_is_int($lung) )  {
         $lung = "20";
      }
      return $lung;
  }    
  function lung_min_art() {
      $lung = $this->lettura('LUNG_ARTICOLO_MIN');
      if ($lung == null || !db_isnumeric($lung) || !db_is_int($lung) )  {
         $lung = "0";
      }
      return $lung;
  }  
  

  function lung_max_mag() {
      $lung = $this->lettura('LUNG_MAGAZZINO_MAX');
      if ($lung == null || !db_isnumeric($lung) || !db_is_int($lung) )  {
         $lung = "20";
      }
      return $lung;
  }    
  
  function lung_min_mag() {
      $lung = $this->lettura('LUNG_MAGAZZINO_MIN');
      if ($lung == null || !db_isnumeric($lung) || !db_is_int($lung) )  {
         $lung = "0";
      }
      return $lung;
  }  


  function max_articoli() {
      $lung = $this->lettura('MAX_ARTICOLI');
      if ($lung == null || !db_isnumeric($lung) || !db_is_int($lung) )  {
         $lung = "99999";
      }
      return $lung;
  }    
  
  function min_articoli() {
      $lung = $this->lettura('MIN_ARTICOLI');
      if ($lung == null || !db_isnumeric($lung) || !db_is_int($lung) )  {
         $lung = "0";
      }
      return $lung;
  } 
    
}

?>