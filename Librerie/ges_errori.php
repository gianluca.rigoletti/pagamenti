<?php

class ges_errori
{

     var $Errori= array();
     var $Errore_campo = array();
     var $Testo_errore_campo = array();
     var $pop_errori = true;
     var  $altra_funzione = "";

     function add($testo,$campo = null) {
         $this->Errori[] = $testo;
         if ($campo != null && $campo != " " && $campo != "") {
             if (is_array($campo)) { 
               for ($i=0;$i<count($campo);$i++) {
                  $this->Errore_campo[] = $campo[$i];
                  $this->Testo_errore_campo[] = $testo;
               }
             } else { 
               $this->Errore_campo[] = $campo;
               $this->Testo_errore_campo[] = $testo;
             }
         }
     }
     
     function set_pop_errori ($var) {
        if (!$var) $this->pop_errori == false;
     }

     function set_altra_funzione ($var) {
        $this->altra_funzione == $var;
     }     
     
     function is_errore() {
         return count($this->Errori) >0;
     }
     
     function mostra() {
       if (isset($this->Errori) && count($this->Errori) > 0) {       
          if (!isset($this->pop_errori) || $this->pop_errori ) {
            echo "
            <div id=\"dialog\" title=\"Attenzione: Rilevate le seguenti segnalazioni\">
            <center><img src=\"../../Icons/Warning.gif\"></center>";
      
            for ($a = 0; $a< count($this->Errori); $a++) { 
                 echo "<span class=\"errore\" >".$this->Errori[$a]."</span><br>";
            }
            
            echo "</div>";
          
            echo "	<script type=\"text/javascript\">
          	        $(function() {
          		       $(\"#dialog\").dialog({
          			       height: 'auto',
                       minHeight: 180,
                       maxHeight: 600,
                       width : 'auto',
                       minWidth: 340, 
                       maxWidth: 900,
                       modal: true,
                       buttons: {
                 				Ok: function() {
                 					$( this ).dialog( \"close\" ); ";
                   if (isset($this->altra_funzione) && $this->altra_funzione != "" ) echo $this->altra_funzione;
                   echo "				}}
          		      } );       
                   	});
                
                	
	                  function apri_errore() {
                       $(\"#dialog\").dialog(\"open\");  
                    }
                   
          	       </script>";
                   
                   
             // echo "<a href=\"javascript:apri_errore();\">Mostra errori</a>";
          } else {
              if (isset($this->Errori) && count($this->Errori) > 0) {                
                  $numero_Errori = count($this->Errori); 
                  echo "<table> <tr><td class=\"px\" height=\"30\"></td></tr><tr>";
                    for ($a = 0; $a< $numero_Errori; $a++) { 
                         echo "<td class=\"errore\">".$this->Errori[$a]."</td></tr>";
                    }
                  echo "<tr><td class=\"px\" height=\"10\"></td></tr> </table>";
              
              } 
          }
        }
      
      }
      
      
      function tooltip($campo,$echo = true) {
      
          $testo = "<ul class=err >";
          $trovato = false;
          for ($i=0;$i<count($this->Errore_campo);$i++) {
              if ($this->Errore_campo[$i] == $campo) {
                 $trovato = true;
                 $testo .= "<li>".$this->Testo_errore_campo[$i]."</li>";
              }
          }
          
          $testo .= "</ul>";
          
          if ($trovato) {
             $stringa =  " onMouseOver=\"toolTip('".$testo."', 300)\" onMouseOut=\"toolTip()\" class=\"Campoerrore\" ";
             if ($echo) echo $stringa;
             else  return $stringa;
          }
      }
}
?>   