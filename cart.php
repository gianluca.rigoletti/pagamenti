<?php
require 'Librerie/html.php';
require 'Librerie/configurazione.php';
require 'ges_cart.php';

$default_path = 'config.json';
//Per poter modificare o meno le righe della tabella
$toggle_modify_table = false;
//Per poter visualizzare le righe del carrello o meno
$toggle_view_table = true;
//Variabili per la visualizzazione della navbar
$shopping_cart_view = 'disabled';
$personal_data_view = 'disabled';
$confirm_data_view = 'disabled';
$payment_view = 'disabled';
//Variabile per la visualizzazione del tasto rurl
$rurl = '';
$view_rurl = false;

function hasValue($value) {
	return (isset($value) && !db_is_null($value));
}

function getSito($value) {
	global $default_path;
	$string = file_get_contents($default_path);
	$json = json_decode($string, true);
	if ($value == -1) {
		return $json['sito'];
	}
	$index = $value;
	return $json['sito'][$index];
}

function getShippingType() {
	global $default_path;
	$string = file_get_contents($default_path);
	$json = json_decode($string, true);
	return $json['shipping-type'];
}

function getSpese() {
	global $default_path;
	$string = file_get_contents($default_path);
	$json = json_decode($string, true);
	return $json['spese'];
}

function modificaBottoniTabella($toggle_modify_table, $id_riga)
{
	if ($toggle_modify_table) {
		return "
		<td><button type='submit' id='upd' name='upd' value='$id_riga' class='btn btn-primary'>Update</button></td>
		<td><button type='submit' id='del' name='del' value='$id_riga' class='btn btn-primary'>Delete</button></td>
		";
	} else {
		return "";
	}
}

function modificaHeaderTabella($toggle_modify_table)
{
	if ($toggle_modify_table) {
		echo "
		<th>Modify</th>
		<th>Remove</th>
		";
	} else {
		echo "";
	}
}

//Controllo se è impostata la sessione
if (isset($_SESSION['id_cart']) && !db_is_null($_SESSION['id_cart'])) {
    //Recupero il carrello dalla sessione impostata
    $carrello  = new Cart($_SESSION['id_cart']);
} else {
    $carrello = new Cart();
}

//Controllo se azionato il post di modifica o delete delle righe
if (isset($_POST['del']) && !db_is_null($_POST['del'])) {
          //Delete
          $carrello->deleteRow($_POST['del']);
}

//Controllo se fare update
if (isset($_POST['upd']) && !db_is_null($_POST['upd'])) {
    $carrello->updateRow($_POST['upd'], $_POST["qta_".$_POST['upd']]);
}

//Condizioni per determinare se sono in visualizzazione o inserimento dei record
$condizione_inserimento = isset($_GET['id']) && isset($_GET['qta']) && isset($_GET['site']);// && isset($_GET['Input']) && $_GET['Input'] == 'Acquista';

//Condizioni per visualizzare i link di navigazione
if (isset($_POST['navigation']) && $_POST['navigation'] == 'personal_data') {
	$personal_data_view = '';
}
if (!isset($_POST['navigation']) || db_is_null($_POST['navigation'])) {
	$shopping_cart_view = '';
}
if (isset($_POST['navigation']) && $_POST['navigation'] == 'payment') {
	$payment_view = '';
}
if (isset($_POST['navigation']) && $_POST['navigation'] == 'confirm_data') {
	$confirm_data_view = '';
}

//Controllo se è inserito il rurl in get o in session quando entro per la prima volta nel carrello
if ($condizione_inserimento) {
  if (isset($_GET['rurl']) && !db_is_null($_GET['rurl'])) {
    $rurl = $_GET['rurl'];
    $_SESSION['rurl']  = $rurl;
    $view_rurl = true;
  }
} else if (isset($_SESSION['rurl']) && !db_is_null($_SESSION['rurl'])) {
  $rurl = $_SESSION['rurl'];
  $view_rurl = true;
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/custom.css">
  </head>
  <body>

  <div class="container">
  	<div class="row">
  		<div class="row">
  			<div class="col-xs-12">
  				<img src="http://www.americanservice.eu/img/logo_store_1.gif" alt="" class="img-fluid pull-xs-left">
  				<img src="http://www.americanservice.eu/img/cart.gif" alt="" class="img-fluid pull-xs-right">
  			</div>
  		</div>
  		<hr>
  		<div class="row">
  			<div class="col-xs-12">
  				<nav class="nav nav-inline ">
  						<a href="" class="nav-link <?php echo $shopping_cart_view; ?>"><span class="h2">1</span> <span>Shopping Cart</span><hr></a>
  						<a href="" class="nav-link <?php echo $personal_data_view; ?>"><span class="h2">2</span> <span>Personal Data</span><hr></a>
  						<a href="" class="nav-link <?php echo $confirm_data_view;  ?>"><span class="h2">3</span> <span>Confirm Data</span><hr></a>
  						<a href="" class="nav-link <?php echo $payment_view;       ?>"><span class="h2">4</span> <span>Payment</span><hr></a>
  						<a href="" class="nav-link disabled"><span class="h2">5</span> <span>Result</span><hr></a>
  				</nav>
  			</div>
  		</div>
  		<div class="row ">

  		<!-- Start of template -->

<?php if (isset($_POST['navigation']) && $_POST['navigation'] == 'personal_data' ): ?>
  <?php
  $nome    = "";
  $cognome = "";
  $email   = "";
  $note    = "";
  if (isset($_SESSION['id_cart']) && !db_is_null($carrello)) {
    $nome     = $carrello->getNome();
    $cognome  = $carrello->getCognome();
    $note     = $carrello->getNote();
    $email    = $carrello->getEmail();
  }
  ?>
    <div class="col-xs-6 center-content">
      <form method="post" action="cart.php" id="user_data">
        <fieldset class="form-group">
          <label for="nome">First Name:</label>
          <input type="text" class="form-control" id="nome" name="nome" value="<?php echo $nome; ?>">
        </fieldset>
        <fieldset class="form-group">
          <label for="cognome">Second Name:</label>
          <input type="text" class="form-control" id="cognome"name="cognome" value="<?php echo $cognome; ?>">
        </fieldset>
        <fieldset class="form-group">
          <label for="email">Email:</label>
          <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>">
        </fieldset>
        <fieldset class="form-group">
          <label for="note">Note:</label>
          <textarea class="form-control" id="note" name="note" rows="3"><?php echo $note; ?></textarea>
        </fieldset>
<?php if ($carrello->hasShippingFees()): ?>

<?php if ($carrello->hasShippingData()) {
	$dati = $carrello->getShippingData();
} else {
	$dati = array(
		'DESTINATARIO' => '',
		'INDIRIZZO'    => '',
		'CAP'          => '',
		'CITTA'        => '',
		'STATO'        => '',
		'NAZIONE'      => '',
		'DESTINATARIO' => '',
	);
}
 ?>
				<h1>Shipping Data</h1>
				<div class="vertical-spacing">
				</div>
				<fieldset class="form-group">
					<label for="tipo">Shipping type:</label>
					<input type="radio" name="tipo" id="tipo" value="1" checked>
					Italy
				</fieldset>
        <fieldset class="form-group">
          <label for="destinatario">Addressee:</label>
          <input type="text" class="form-control" id="destinatario"name="destinatario" value="<?php echo $dati['DESTINATARIO']; ?>">
        </fieldset>
        <fieldset class="form-group">
          <label for="indirizzo">Address:</label>
          <input type="text" class="form-control" id="indirizzo" name="indirizzo" value="<?php echo $dati['INDIRIZZO']; ?>">
        </fieldset>
				<fieldset class="form-group">
          <label for="cap">Zip code:</label>
          <input type="number" class="form-control" id="cap" name="cap" value="<?php echo $dati['CAP']; ?>" maxlength="5">
        </fieldset>
				<fieldset class="form-group">
          <label for="citta">City:</label>
          <input type="text" class="form-control" id="citta" name="citta" value="<?php echo $dati['CITTA']; ?>" >
        </fieldset>
				<fieldset class="form-group">
          <label for="stato">State:</label>
          <input type="text" class="form-control" id="stato" name="stato" value="<?php echo $dati['STATO']; ?>" >
        </fieldset>
				<fieldset class="form-group">
          <label for="nazione">Country:</label>
          <input type="text" class="form-control" id="nazione" name="nazione" value="<?php echo $dati['NAZIONE']; ?>" >
        </fieldset>

<?php endif; ?>
        <button type="submit" class="btn btn-primary cancel" id="back" name="navigation" value="">Back</button>
        <button type="submit" class="btn btn-primary" id="next" name="navigation" value="confirm_data">Next</button>
      </form>
    </div>
<?php endif ?>

<?php if (isset($_POST['navigation']) && ($_POST['navigation']) == 'payment'): ?>
<?php
	$toggle_view_table = false;
	//Salvo il carrello nel db
	Cart::saveToDatabase($carrello);
	//$_SESSION['carrello'] = $carrello;
  //Set the payment session
  $_SESSION['payment'] = 1;
        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        // if (ini_get("session.use_cookies")) {
        //     $params = session_get_cookie_params();
        //     setcookie(session_name(), '', time() - 42000,
        //         $params["path"], $params["domain"],
        //         $params["secure"], $params["httponly"]
        //     );
        // }

        // Finally, destroy the session.
        //session_destroy();
        //die(print_r($_SESSION));
        echo "<script>window.location.href= 'pagamento-setefi.php'</script>";

?>

<!--	<meta http-equiv="refresh" content="3;url=http://google.com">-->
	<h3> Redirect to ...</h3>

<?php endif ?>

<?php if ($toggle_view_table): ?>
<?php if(!isset($_POST['navigation']) || db_is_null($_POST['navigation']) || ( isset($_GET['Action']) && $_GET['Action'] == 'View' )) $toggle_modify_table = true; ?>

			<div class="row" style="height:50px;"></div>
			<form method="post" action="cart.php?Action=View" id="upd">
				<div class="col-xs-12 center-content">
					<h3 id="shopping-cart"> Shopping Cart </h3>
					<div class="table-reponsive">
						<table class="table table-sm">
							<thead class="thead-inverse">
								<tr>
									<th>Id</th>
									<th>Product</th>
									<th>Quantity</th>
									<th>Cost</th>
									<th>Total</th>
									<?php
									modificaHeaderTabella($toggle_modify_table);
									?>
								</tr>
							</thead>
							<tbody>
								<?php
								$totale = 0;
								$desc = '';
								$price = '';
								if ($condizione_inserimento) {
									if(hasValue($_GET['id']) && hasValue($_GET['qta']) && hasValue($_GET['site'])) {
										//Inserimento Riga nel carrello. Controllo se sto inserendo una riga con id = 0 o meno
										if (isset($_GET['description']) && !db_is_null($_GET['description']) && isset($_GET['price']) && !db_is_null($_GET['price'])) {
											$desc  = base64_decode($_GET['description']);
											$price = base64_decode($_GET['price']);
											$carrello->insertRow($_GET['site'], $_GET['id'], $_GET['qta'], $desc, $price);
										} else {
											$carrello->insertRow($_GET['site'], $_GET['id'], $_GET['qta']);
										}
									}
								}
									//Visualizzazione delle righe
									$righe = $carrello->getRows();
									foreach ($righe as $riga) {
										$prodotto = '';
										$qta = 0;
										$prezzo = 0;
										$sub_totale = 0;
										$spese_spedizione = 0;
										$id_pagamenti  = $riga['IDPAGAMENTI'];
										$id_riga = $riga['ID'];
										$query_riga = db_query_mod('pagamenti', $id_pagamenti);
										$res_riga = mysql_fetch_assoc($query_riga);
										//Controllo se devo visualizzare riga con id= 0
                    $readonly = ($id_pagamenti == 0) ? 'readonly="readonly"' : '' ;
										if ($id_pagamenti == 0) {
											$id_pagamenti = '';
											$prodotto = $riga['DESCRIZIONE'];
											$prezzo  = $riga['PREZZO'];
										} else {
											$prodotto = $res_riga['PRODOTTO'];
											$prezzo = $res_riga['PREZZO'];
										}
										$qta = $riga['QTA'];
										$spese_spedizione = ($res_riga['SPESE_SPEDIZIONE'] == 0) ? 0 : getSpese() ;
										$sub_totale = $prezzo*$qta + $spese_spedizione;
										$totale += $sub_totale;
										$modifica_quantita = ($toggle_modify_table) ? '' : 'disabled' ;
										$modifica_cancella = modificaBottoniTabella($toggle_modify_table, $id_riga);
										echo "
										<tr>
											<th scope='row'> ".$id_pagamenti." </th>
											<input type='hidden' id='id' name='$id_riga' value='$id_riga' >
											<td> ".$prodotto." </td>
											<td><input type='text' class='form-control' value='$qta' name='qta_$id_riga' id='qta' $modifica_quantita ".$readonly."></td>
											<td> ".db_visimporti($prezzo)." </td>
											<td> ".db_visimporti($sub_totale)." </td>
											$modifica_cancella
										</tr>
										";
									}
								?>
							</tbody>
						</table>
					</div>
					<h3 class="pull-xs-right">Total: <?php echo db_visimporti($carrello->getTotal()); ?></h3>
					<div class="row" style="height: 70px;"></div>
				</div>
			</form>
			<form method="post" action="cart.php?Action=View">
				<div class="row">
					<div class="col-xs-12 center-content">
						<!-- Buttons for next action -->
						<?php if ( (!isset($_POST['navigation']) || db_is_null($_POST['navigation']) || ( isset($_GET['Action']) && $_GET['Action'] == 'View'))  ) $visible = ''; else $visible = 'hidden'; ?>
						<?php if ( (isset($_POST['navigation']) && $_POST['navigation'] == 'personal_data' ) && (isset($_GET['Action']) && $_GET['Action'] == 'View' ) ) $visible = 'hidden'; ?>
						<button type="submit" class="btn btn-primary" id="navigation" name="navigation" value="personal_data" <?php echo $visible; ?>>Next</button>
					</div>
				</div>
			</form>

<?php endif ?>
<?php if (isset($_POST['navigation']) && ($_POST['navigation']) == 'confirm_data'): ?>

<?php
	//Riempio l'oggetto carrello. Se poi l'utente prosegue in payment salvo il carrello nel db
	$carrello->setNome($_POST['nome']);
	$carrello->setCognome($_POST['cognome']);
	$carrello->setEmail($_POST['email']);
	$carrello->setNote($_POST['note']);
	//$_SESSION['carrello'] = $carrello;

	//Se impostati salvo i dati relativi alla spedizione
	if($carrello->hasShippingFees()) {
		$dati  = array(
			'TIPO'         => $_POST['tipo'],
			'DESTINATARIO' => $_POST['destinatario'],
			'INDIRIZZO'    => $_POST['indirizzo'],
			'CAP'          => $_POST['cap'],
			'CITTA'        => $_POST['citta'],
			'NAZIONE'      => $_POST['nazione'],
			'STATO'        => $_POST['stato']
		);
		$carrello->setShippingData($dati);
	}

	Cart::saveToDatabase($carrello);
?>

	    			<div class="col-xs-8 center-content">
	    				<h2>Personal Data</h2>
	    				<table class="table">
	    					<tbody>
	    						<tr>
	    							<th scope="row">First Name</th>
	    							<td><?php echo $_POST['nome']; ?></td>
	    						</tr>
	    						<tr>
	    							<th scope="row">Last Name</th>
	    							<td><?php echo $_POST['cognome']; ?></td>
	    						</tr>
	    						<tr>
	    							<th scope="row">Email</th>
	    							<td> <?php echo $_POST['email']; ?> <td>
	    						</tr>
	    						<tr>
	    							<th scope="row">Notes</th>
	    							<td><?php echo $_POST['note']; ?></td>
	    						</tr>
	    					</tbody>
	    				</table>
							<div class="vertical-spacing"></div>
<?php if ($carrello->hasShippingData()): ?>
							<h2>Shipping Data</h2>
							<table class="table">
								<tbody>
									<tr>
										<th scope="row">Shipping Type</th>
										<td><?php echo getShippingType($_POST['tipo']); ?></td>
									</tr>
									<tr>
										<th scope="row">Addressee</th>
										<td><?php echo $_POST['destinatario']; ?></td>
									</tr>
									<tr>
										<th scope="row">Address</th>
										<td> <?php echo $_POST['indirizzo']; ?> <td>
									</tr>
									<tr>
										<th scope="row">Zip code</th>
										<td><?php echo $_POST['cap']; ?></td>
									</tr>
									<tr>
										<th scope="row">City</th>
										<td><?php echo $_POST['citta']; ?></td>
									</tr>
									<tr>
										<th scope="row">State</th>
										<td><?php echo $_POST['stato']; ?></td>
									</tr>
									<tr>
										<th scope="row">Country</th>
										<td><?php echo $_POST['nazione']; ?></td>
									</tr>
								</tbody>
							</table>
<?php endif; ?>

	    				<form method="post">
	    						<button type="submit" class="btn btn-primary" name="navigation" value="personal_data">Back</button>
	    						<button type="submit" class="btn btn-primary" name="navigation" value="payment">Next</button>
	    				</form>
	    			</div>

<?php endif ?>
<?php if ($view_rurl): ?>
        <div class="vertical-spacing">
        </div>
        <div class="row">
          <div class="col-xs-4 col-xs-offset-5">
            <a href=" <?php echo $rurl ?> " class="btn btn-primary btn-large">Main Page</a>
          </div>
        </div>
<?php endif; ?>

    			<!-- End of template -->
    		</div>
    		<div class="row" style="height:100px;"></div>
    	</div>
    </div>

<!--     <footer class="footer">
        <div class="container">
        	<span class="text-muted">Place sticky footer content here.</span>
        </div>
    </footer> -->

    <!-- jQuery first, then Bootstrap JS. -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="Html/jquery.validate.min.js"></script>
      <script type="text/javascript">
          jQuery.extend(jQuery.validator.messages, {
            required: "Campo Obbligatorio",
            email: "Inserire una mail valida",
            digits: "Inserire solo numeri",
            importoeuro:"Inserire un importo corretto",
            percentuale:"Inserire una percentuale tra 1 e 99",
            maxlength:"Inserire al massimo {0} caratteri",
            minlength:"Inserire almeno {0} caratteri"
        });
	  $.validator.methods.importoeuro = function(value, element, param) {
	  	if (value == null || value == "" ) return true;
	  	value =  value.replace(".","");
	  	var myre= /^\d+([\,]{1}\d{2})?$/;
	  	if (!(myre.test(value))) {
	  		return false;
	  	}
	  	var imp = value.replace(".","");
	  	imp = imp.replace(",",".");
	  	var impN = parseFloat(imp);

	  	if (impN < 0) return false;


	  	return true;

	  };
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
    <script>
	    function b64EncodeUnicode(str) {
	        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
	            return String.fromCharCode('0x' + p1);
	        }));
	    }

		var validator_insert;
		var validator_user;
		var validator_rec;

		$().ready(function($) {

   	    validator_insert = $("form#insert").validate({
		    ignore : [],
		    submitHandler: function(form) {
	    	var id = $('form#insert').children('input#id').val();
	    	var qta = $('#qta').val();
	    	var description  = b64EncodeUnicode($('#desc').val());
	    	var price = b64EncodeUnicode($('#price').val());
	    	var site =$('#site').val();
	    	window.location.href = 'cart.php?id=' + id + '&qta=' + qta + '&description=' + description + '&site=' + site + '&price=' + price + '&Input=Acquista';
		    //form.submit();
		},
		rules:  {
		    	desc: { required: true },
		    	qta: { required: true, digits: true, min: 1 },
		    	price: { importoeuro: true }
		    }
		});

   	    validator_user = $("form#user_data").validate({
		    ignore : [],
		    submitHandler: function(form) {
		    form.submit();
		},
		rules:  {
		    	nome: { required: true },
		    	cognome: { required: true },
		    	email: { required: true, email:true },
					tipo: { required: true },
					destinatario: { required: true },
					indirizzo: { required: true },
					cap: {
						required: true,
						maxlength: 5,
						digits: true
					},
					citta: { required: true },
					nazione: { required: true },
					stato: { required: true }
		    }
		});

		});

    </script>
  </body>
</html>
